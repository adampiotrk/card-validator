package sda.creditcards;

import org.junit.Test;
import sda.creditcards.luhnvalidator.impl.SDACardValidator;
import sda.creditcards.luhnvalidator.ValidationResult;

import static org.junit.Assert.assertEquals;

public class AppTest {

    @Test
    public void should_give_issues_and_check_validation() {
        String cardLuhnPassedNoIssuer = "18606";
        String cardNoIssuer = "102345";
        String cardMasterCard = "5390 4139 1740 1103";
        String cardAmericanExpress = "375298921824600";
        String cardVisa = "4652 6661 0071 1804";
        String path = "C:/Users/adamk/IdeaProjects/validator/src/main/java/sda/creditcards/CardParameters.txt";

        ValidationResult validationResultLuhnPassed = new SDACardValidator().validateCardNo(cardLuhnPassedNoIssuer, path);
        ValidationResult validationResultVisa = new SDACardValidator().validateCardNo(cardVisa, path);
        ValidationResult validationResultMasterCard = new SDACardValidator().validateCardNo(cardMasterCard, path);
        ValidationResult validationResultAmericanExpress = new SDACardValidator().validateCardNo(cardAmericanExpress, path);
        ValidationResult validationResultNotPassed = new SDACardValidator().validateCardNo(cardNoIssuer, path);

        assertEquals(true, validationResultLuhnPassed.isLuhnPassed());
        assertEquals(true, validationResultVisa.isLuhnPassed());
        assertEquals(true, validationResultMasterCard.isLuhnPassed());
        assertEquals(true, validationResultAmericanExpress.isLuhnPassed());
        assertEquals(false, validationResultNotPassed.isLuhnPassed());

        assertEquals("Nie znaleziono wystawcy karty w bazie", validationResultLuhnPassed.getIssuer());
        assertEquals("Nie znaleziono wystawcy karty w bazie", validationResultNotPassed.getIssuer());
        assertEquals("Visa", validationResultVisa.getIssuer());
        assertEquals("MasterCard", validationResultMasterCard.getIssuer());
        assertEquals("AmericanExpress", validationResultAmericanExpress.getIssuer());

    }
}

