package sda.creditcards;


import org.junit.Test;
import sda.creditcards.issuer.CardParameters;
import sda.creditcards.issuer.CardParametersBuilder;
import sda.creditcards.issuer.CardParametersReader;
import sda.creditcards.issuer.ICardParameters;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class CardParametersReaderTest {

    @Test
    public void should_read_parameters_from_file() {
        List<CardParameters> cardParametersReader = CardParametersBuilder.produce("C:/Users/adamk/IdeaProjects/validator/src/main/java/sda/creditcards/CardParameters.txt");
        String[] line;
        assertEquals(8, cardParametersReader.size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void should_throw_exception() {
        List<CardParameters> cardParametersReader = CardParametersBuilder.produce(null);
        String[] line;

    }


}