package sda.creditcards;

import org.junit.Test;
import sda.creditcards.issuer.IIssuerDetector;
import sda.creditcards.issuer.IssuerDetectorImpl;

import static org.junit.Assert.assertEquals;

public class IssuerDetectorImplTest {

    @Test
    public void should_return_true() {
        String cardNo = "18606";
        String cardMasterCard = "5390 4139 1740 1103";
        String cardAmericanExpress = "375298921824600";
        String cardVisa = "4652 6661 0071 1804";
        String path = "C:/Users/adamk/IdeaProjects/validator/src/main/java/sda/creditcards/CardParameters.txt";

        IIssuerDetector issuerDetector = new IssuerDetectorImpl();
        assertEquals("Nie znaleziono wystawcy karty w bazie", issuerDetector.detectIssuer(cardNo, path));
        assertEquals("Visa", issuerDetector.detectIssuer(cardVisa, path));
        assertEquals("MasterCard", issuerDetector.detectIssuer(cardMasterCard, path));
        assertEquals("AmericanExpress", issuerDetector.detectIssuer(cardAmericanExpress, path));


    }
}

