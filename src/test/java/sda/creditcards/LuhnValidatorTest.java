package sda.creditcards;

import org.junit.Test;
import sda.creditcards.luhnvalidator.impl.LuhnValidatorImpl;

import static org.junit.Assert.assertEquals;

public class LuhnValidatorTest {

    @Test
    public void should_return_true() {
        String cardNo = "18606";
        String cardMasterCard = "5390 4139 1740 1103";
        String cardAmericanExpress = "375298921824600";
        String cardVisa = "4652 6661 0071 1804";

        LuhnValidatorImpl validator = new LuhnValidatorImpl();
        assertEquals(true, validator.verify(cardNo));
        assertEquals(true, validator.verify(cardMasterCard));
        assertEquals(true, validator.verify(cardAmericanExpress));
        assertEquals(true, validator.verify(cardVisa));

    }

    @Test
    public void should_return_false() {
        String cardNo = "18605";
        LuhnValidatorImpl validator = new LuhnValidatorImpl();
        assertEquals(false, validator.verify(cardNo));

    }

}