package sda.creditcards;


import sda.creditcards.luhnvalidator.impl.SDACardValidator;
import sda.creditcards.luhnvalidator.ValidationResult;

import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        String pathToFile = "C:/Users/adamk/IdeaProjects/validator/src/main/java/sda/creditcards/CardParameters.txt";

        System.out.println("Put card number:");
        Scanner scanner = new Scanner(System.in);
        String cardNo = scanner.nextLine();

        ValidationResult validationResult = new SDACardValidator().validateCardNo(cardNo, pathToFile);


        System.out.println("Card issuer: " + validationResult.getIssuer());
        System.out.println("Luhn validation passed: " + validationResult.isLuhnPassed());
    }
}
