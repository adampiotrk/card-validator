package sda.creditcards.luhnvalidator.impl;

import sda.creditcards.luhnvalidator.ILuhnValidator;

public class LuhnValidatorImpl implements ILuhnValidator {
    public boolean verify(String cardNo) {
        int[] numbers = getInts(cardNo);
        int result = calculateSumOfLuhnAlgorithm(numbers);

        if (result % 10 == 0) {
            return true;
        } else {
            return false;
        }
    }

    private int calculateSumOfLuhnAlgorithm(int[] numbers) {

        for (int i = numbers.length - 2; i >= 0; i = i - 2) {
            if (numbers[i] * 2 < 10) {
                numbers[i] = numbers[i] * 2;
            } else {
                numbers[i] = (numbers[i] * 2) % 10 + 1;
            }
        }

        int sum = 0;
        for (int i = 0; i < numbers.length; i++) {
            sum = sum + numbers[i];
        }
        return sum;
    }

    private int[] getInts(String cardNo) {
        int[] numbers = cardNo
                .replace(" ", "")
                .chars()
                .toArray();
        for (int i = 0; i < numbers.length; i++) {
            numbers[i] -= 48;
        }
        return numbers;
    }

}
