package sda.creditcards.luhnvalidator.impl;

import sda.creditcards.issuer.IIssuerDetector;
import sda.creditcards.issuer.IssuerDetectorImpl;
import sda.creditcards.luhnvalidator.ICardValidator;
import sda.creditcards.luhnvalidator.ILuhnValidator;
import sda.creditcards.luhnvalidator.ValidationResult;

public class SDACardValidator implements ICardValidator {


    @Override
    public ValidationResult validateCardNo(String cardNo, String path) {
        ValidationResult result = new ValidationResult(path);

        ILuhnValidator luhnValidator = new LuhnValidatorImpl();
        boolean isCorrect = luhnValidator.verify(cardNo);

        IIssuerDetector detector = new IssuerDetectorImpl();
        String issuer = detector.detectIssuer(cardNo,path);

        result.setLuhnPassed(isCorrect);
        result.setIssuer(issuer);
        return result;
    }
}
