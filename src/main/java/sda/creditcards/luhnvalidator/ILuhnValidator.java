package sda.creditcards.luhnvalidator;

public interface ILuhnValidator {
    boolean verify(String cardNo);
}
