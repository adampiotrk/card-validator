package sda.creditcards.luhnvalidator;

public class ValidationResult {
    private String issuer;
    private boolean isLuhnPassed = false;
    private String path;

    public ValidationResult(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getIssuer() {
        return issuer;
    }

    public void setIssuer(String issuer) {
        this.issuer = issuer;
    }

    public boolean isLuhnPassed() {
        return isLuhnPassed;
    }

    public void setLuhnPassed(boolean luhnPassed) {
        this.isLuhnPassed = luhnPassed;
    }
}
