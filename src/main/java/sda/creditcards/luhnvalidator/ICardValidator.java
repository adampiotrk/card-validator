package sda.creditcards.luhnvalidator;

import sda.creditcards.luhnvalidator.ValidationResult;

public interface ICardValidator {
    ValidationResult validateCardNo(String cardNo, String path);
}
