package sda.creditcards.issuer;

import java.util.List;

public class IssuerDetectorImpl implements IIssuerDetector {

    @Override
    public String detectIssuer(String cardNo, String path) {

        List<CardParameters> cardParametersList = CardParametersBuilder.produce(path);

        for (int i = 0; i < cardParametersList.size(); i++) {
            Integer prefix = cardParametersList.get(i).getPrefix();
            Integer length = cardParametersList.get(i).getLenght();
            if (cardNo.startsWith(prefix.toString()) && cardNo.replace(" ", "").length() == length) {
                return cardParametersList.get(i).getCompany();
            }
        }
        return "Nie znaleziono wystawcy karty w bazie";
    }
}
