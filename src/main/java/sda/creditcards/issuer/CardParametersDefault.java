package sda.creditcards.issuer;

import java.util.ArrayList;
import java.util.List;

public class CardParametersDefault implements ICardParameters {
    @Override
    public List<CardParameters> buildRules() {
        List<CardParameters> issuerRules = new ArrayList<>();

        CardParameters rule1 = new CardParameters();
        rule1.setCompany("Visa");
        rule1.setLenght(16);
        rule1.setPrefix(4);

        CardParameters rule2 = new CardParameters();
        rule1.setCompany("MasterCard");
        rule1.setLenght(16);
        rule1.setPrefix(51);

        CardParameters rule3 = new CardParameters();
        rule1.setCompany("MasterCard");
        rule1.setLenght(16);
        rule1.setPrefix(52);

        CardParameters rule4 = new CardParameters();
        rule1.setCompany("MasterCard");
        rule1.setLenght(16);
        rule1.setPrefix(53);

        CardParameters rule5 = new CardParameters();
        rule1.setCompany("MasterCard");
        rule1.setLenght(16);
        rule1.setPrefix(54);

        CardParameters rule6 = new CardParameters();
        rule1.setCompany("MasterCard");
        rule1.setLenght(16);
        rule1.setPrefix(55);

        CardParameters rule7 = new CardParameters();
        rule1.setCompany("American Express");
        rule1.setLenght(15);
        rule1.setPrefix(34);

        CardParameters rule8 = new CardParameters();
        rule1.setCompany("American Express");
        rule1.setLenght(15);
        rule1.setPrefix(37);

        issuerRules.add(rule1);
        issuerRules.add(rule2);
        issuerRules.add(rule3);
        issuerRules.add(rule4);
        issuerRules.add(rule5);
        issuerRules.add(rule6);
        issuerRules.add(rule7);
        issuerRules.add(rule8);
        return issuerRules;
    }
}
