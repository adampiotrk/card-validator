package sda.creditcards.issuer;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CardParametersReader implements ICardParameters {
    private String path;

    public CardParametersReader(String path) {
        this.path = path;
    }

    @Override
    public List<CardParameters> buildRules() {

        try {
            File file = new File(path);
            Scanner scanner = new Scanner(file);
            List<CardParameters> cardParametersList = new ArrayList<>();

            while (scanner.hasNextLine()) {
                CardParameters cardParameters = new CardParameters();
                String[] line = scanner.nextLine().split(";");
                cardParameters.setCompany(line[0]);
                cardParameters.setPrefix(Integer.parseInt(line[1]));
                cardParameters.setLenght(Integer.parseInt(line[2]));
                cardParametersList.add(cardParameters);
            }
            return cardParametersList;

        } catch (FileNotFoundException n) {
            throw new IllegalArgumentException("Niepoprawna sciezka pliku");
        }
    }

}
