package sda.creditcards.issuer;

import java.util.List;

public class CardParametersBuilder {

    public static List<CardParameters> produce(String filePath) {
        ICardParameters rulesBuilder = null;
        if (filePath != null && !filePath.isEmpty()) {
            rulesBuilder = new CardParametersReader(filePath);
        } else {
            rulesBuilder = new CardParametersDefault();
        }
        return rulesBuilder.buildRules();
    }
}
