package sda.creditcards.issuer;

public interface IIssuerDetector {
    String detectIssuer(String cardNo, String path);

}
