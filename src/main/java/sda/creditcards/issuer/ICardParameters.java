package sda.creditcards.issuer;

import java.util.List;

public interface ICardParameters {
    List<CardParameters> buildRules();
}
